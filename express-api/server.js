const app = require("./app");
let server;

function setServer() {
  // set port, listen for requests
  const PORT = process.env.NODE_DOCKER_PORT || 8080;
  server = app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
  });
}

async function stopService() {
  await server.close();
}

setServer();

module.exports = { stopService };